from celery import task
from wemo import wemo

@task
def wemo_on():
    wemo.msearch(0, 0, wemo.conn, 4)
    wemo.on()
    return wemo.get()

@task
def wemo_off():
    wemo.msearch(0, 0, wemo.conn, 4)
    wemo.off()
    return wemo.get()
