from django.conf.urls import patterns, include, url

urlpatterns = patterns('remotectl.views',
    url(r'^on/$', 'on', name='on'),
    url(r'^off/$', 'off', name='off'),
    url(r'^get/$', 'get', name='get'),
)
