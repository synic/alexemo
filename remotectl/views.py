from django import http
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse

from wemo import wemo

def _get_status(res):
    if res == True:
        return "Alexia is On"
    elif res == False:
        return "Alexia is Off"
    return str(res)

def _request(request, func, onecall=False):
    try:
        if not onecall: func()
        status = _get_status(wemo.get())
        return http.HttpResponse(status, mimetype='text/plain')
    except Exception, e:
        return http.HttpResponse("Alexia is having trouble: %s" % e,
            mimetype='text/plain')

@login_required
def on(request):
    return _request(request, wemo.on)

@login_required
def off(request):
    return _request(request, wemo.off)

@login_required
def get(request):
    return _request(request, wemo.get, True)
