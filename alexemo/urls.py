from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.site.index_template = 'remotectl/admin.html'
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^', include(admin.site.urls)),
    url(r'^remotectl/', include('remotectl.urls', namespace='remotectl')),
)
